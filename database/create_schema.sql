drop schema if exists logically;
create schema if not exists logically;
use logically;

create table categories
(
	category_id int auto_increment
		primary key,
	name varchar(30) not null
);

create table difficulty
(
	difficulty_id int auto_increment
		primary key,
	value varchar(30) not null,
	points int null,
	constraint difficulty_value_uindex
		unique (value)
);

create table status
(
	status_id int auto_increment
		primary key,
	value varchar(30) not null,
	constraint statuses_value_uindex
		unique (value)
);

create table users
(
	username varchar(50) not null
		primary key,
	password varchar(68) not null,
	enabled tinyint not null
);

create table authorities
(
	username varchar(50) not null,
	authority varchar(50) not null,
	constraint authorities_users_username_fk
		foreign key (username) references users (username)
);

create table user_details
(
	user_id int auto_increment
		primary key,
	username varchar(50) not null,
	picture longblob null,
	email varchar(80) not null,
	first_name varchar(20) null,
	last_name varchar(20) null,
	score int null,
	constraint user_details_email_uindex
		unique (email),
	constraint user_details_username_uindex
		unique (username),
	constraint user_details_users_username_fk
		foreign key (username) references users (username)
);

create table problems
(
	problem_id int auto_increment
		primary key,
	name varchar(50) not null,
	description text not null,
	constraints varchar(200) null,
	solution varchar(50) not null,
	explanation text not null,
	category_id int not null,
	status_id int not null,
	difficulty_id int not null,
	creator_id int not null,
	points int not null,
	constraint problems_categories_category_id_fk
		foreign key (category_id) references categories (category_id),
	constraint problems_difficulty_difficulty_id_fk
		foreign key (difficulty_id) references difficulty (difficulty_id),
	constraint problems_status_status_id_fk
		foreign key (status_id) references status (status_id),
	constraint problems_user_details_user_id_fk
		foreign key (creator_id) references user_details (user_id)
);

create table comments
(
	comment_id int auto_increment
		primary key,
	description text not null,
	problem_id int not null,
	creator_id int not null,
	constraint comments_problems_problem_id_fk
		foreign key (problem_id) references problems (problem_id),
	constraint comments_user_details_user_id_fk
		foreign key (creator_id) references user_details (user_id)
);

create table problems_likes
(
	problems_likes_id int auto_increment
		primary key,
	problem_id int not null,
	user_id int not null,
	constraint problems_likes_problems_problem_id_fk
		foreign key (problem_id) references problems (problem_id),
	constraint problems_likes_user_details_user_id_fk
		foreign key (user_id) references user_details (user_id)
);

create table solved_problems_users
(
	id int auto_increment,
	user_id int not null,
	problem_id int not null,
	constraint solved_problems_users_id_uindex
		unique (id),
	constraint solved_problems_users_problems_problem_id_fk
		foreign key (problem_id) references problems (problem_id),
	constraint solved_problems_users_user_details_user_id_fk
		foreign key (user_id) references user_details (user_id)
);

alter table solved_problems_users
	add primary key (id);

