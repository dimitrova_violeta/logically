package com.vdimitrova.logically.utils;

public class GlobalConstants {

    public static final String USERNAME_REQUIRED_ERROR = "Username is required!";
    public static final int USERNAME_MIN_LENGTH = 3;
    public static final int USERNAME_MAX_LENGTH = 30;
    public static final String USERNAME_MESSAGE_ERROR =
            "Username should be between " + USERNAME_MIN_LENGTH + " and " + USERNAME_MAX_LENGTH + " symbols.";

    public static final String EMAIL_REQUIRED_ERROR  = "Email is required!";
    public static final String EMAIL_FORMAT_ERROR = "Not valid email format.";

    public static final int FIRST_NAME_MIN_LENGTH = 3;
    public static final int FIRST_NAME_MAX_LENGTH = 30;
    public static final String FIRST_NAME_MESSAGE_ERROR =
            "First name should be between " + FIRST_NAME_MIN_LENGTH + " and " + FIRST_NAME_MAX_LENGTH + " symbols.";

    public static final int LAST_NAME_MIN_LENGTH = 3;
    public static final int LAST_NAME_MAX_LENGTH = 30;
    public static final String LAST_NAME_MESSAGE_ERROR =
            "Last name should be between " + LAST_NAME_MIN_LENGTH + " and " + LAST_NAME_MAX_LENGTH + " symbols.";

    public static final String PASSWORD_REQUIRED_ERROR = "Password is required!";
    public static final String PASSWORD_CONFIRMATION_REQUIRED_ERROR = "Confirm password is required!";
    public static final String PASSWORD_DOESNT_MATCH_ERROR = "Password confirmation doesn't match password!";
    public static final String PASSWORD_MESSAGE_ERROR = "Password should be at least 8 characters and should contain " +
            "a minimum 1 lower case letter, 1 upper case letter, 1 numeric character and a special character: [@,#,$,%,!]";


    public static final String USERNAME_EXISTS_ERROR = "User with username %s already exists.";
    public static final String EMAIL_EXISTS_ERROR = "User with email %s already exists.";
    public static final String EMPTY_FIELDS_ERROR = "Please add valid value.";

    public static final String USERNAME_NOT_FOUND_ERROR = "User with username %s not found.";

    public static final String CORRECT_SOLUTION_MESSAGE = "Correct solution. You win %d points";
    public static final String SOLUTION_MESSAGE = "Correct solution.";
    public static final String INCORRECT_SOLUTION_MESSAGE = "Your answer is incorrect.";
}
