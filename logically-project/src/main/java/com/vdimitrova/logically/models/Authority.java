package com.vdimitrova.logically.models;

import javax.persistence.*;

@Entity
@IdClass(AuthorityId.class)
@Table(name = "authorities")
public class Authority {

    @Id
    @Column(name = "username")
    private String username;

    @Id
    @Column(name = "authority")
    private String authority;

    public Authority() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        if(authority.equals("ROLE_USER")) {
            return "User";
        } else if(authority.equals("ROLE_MASTER")) {
            return "Master";
        } else {
            return "Admin";
        }
    }
}
