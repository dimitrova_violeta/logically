package com.vdimitrova.logically.models.mappers;

import com.vdimitrova.logically.exceptions.FileStorageException;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.models.dto.UserDtoEditProfile;
import com.vdimitrova.logically.models.dto.UserDtoRegistration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class UserMapper {

    @Autowired
    public UserMapper() {
    }

    public UserDetails toUserDetails(UserDtoRegistration userDtoRegistration) {
        UserDetails userDetails = new UserDetails();
        userDetails.setUsername(userDtoRegistration.getUsername());
        userDetails.setEmail(userDtoRegistration.getEmail());
        userDetails.setFirstName(userDtoRegistration.getFirstName());
        userDetails.setLastName(userDtoRegistration.getLastName());
        return userDetails;
    }

    public void toUserDetails(UserDetails userDetails, UserDtoEditProfile userDtoEditProfile) {
        try {
            userDetails.setFirstName(userDtoEditProfile.getFirstName());
            userDetails.setLastName(userDtoEditProfile.getLastName());
            userDetails.setEmail(userDtoEditProfile.getEmail());
            MultipartFile file = userDtoEditProfile.getPicture();
            if(!file.isEmpty()) {
                userDetails.setPicture(file.getBytes());
            }
        } catch (IOException | NullPointerException ex) {
            throw new FileStorageException(ex.getMessage());
        }
    }
}
