package com.vdimitrova.logically.models;

import java.io.Serializable;
import java.util.Objects;

public class AuthorityId implements Serializable {

    private String username;
    private String authority;

    public AuthorityId() {
    }

    public AuthorityId(String username, String authority) {
        this.username = username;
        this.authority = authority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, authority);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        AuthorityId that = (AuthorityId) obj;
        return Objects.equals(username, that.username) &&
                Objects.equals(authority, that.authority);
    }
}
