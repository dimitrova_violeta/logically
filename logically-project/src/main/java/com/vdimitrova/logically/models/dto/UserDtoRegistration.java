package com.vdimitrova.logically.models.dto;

import com.vdimitrova.logically.annotations.Password;
import com.vdimitrova.logically.annotations.PasswordMatch;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import static com.vdimitrova.logically.utils.GlobalConstants.*;

@PasswordMatch(firstPassword = "password", secondPassword = "passwordConfirmation", message = PASSWORD_DOESNT_MATCH_ERROR)
public class UserDtoRegistration {

    @NotBlank(message = USERNAME_REQUIRED_ERROR)
    @Size(min = USERNAME_MIN_LENGTH, max = USERNAME_MAX_LENGTH, message = USERNAME_MESSAGE_ERROR)
    private String username;

    @NotBlank(message = EMAIL_REQUIRED_ERROR)
    @Email(message = EMAIL_FORMAT_ERROR)
    private String email;

    @Size(min = FIRST_NAME_MIN_LENGTH, max = FIRST_NAME_MAX_LENGTH, message = FIRST_NAME_MESSAGE_ERROR)
    private String firstName;

    @Size(min = LAST_NAME_MIN_LENGTH, max = LAST_NAME_MAX_LENGTH, message = LAST_NAME_MESSAGE_ERROR)
    private String lastName;

    @NotBlank(message = PASSWORD_REQUIRED_ERROR)
    @Password(message = PASSWORD_MESSAGE_ERROR)
    private String password;

    @NotBlank(message = PASSWORD_CONFIRMATION_REQUIRED_ERROR)
    private String passwordConfirmation;

    public UserDtoRegistration() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
