package com.vdimitrova.logically.models.dto;

public class ProblemDtoUserSolution {

    private String userSolution;

    public ProblemDtoUserSolution() {
    }

    public String getUserSolution() {
        return userSolution;
    }

    public void setUserSolution(String userSolution) {
        this.userSolution = userSolution;
    }
}
