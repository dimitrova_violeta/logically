package com.vdimitrova.logically;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogicallyApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogicallyApplication.class, args);
    }

}
