package com.vdimitrova.logically.controllers.mvc;

import com.vdimitrova.logically.exceptions.DuplicateEntityException;
import com.vdimitrova.logically.exceptions.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = "com.vdimitrova.logically.controllers.mvc")
public class ExceptionHandlerMvcController {

    @ExceptionHandler(value = {DuplicateEntityException.class})
    @ResponseStatus(code = HttpStatus.CONFLICT)
    public String exceptionDuplicateEntityException (RuntimeException runtimeException, Model model) {
        String errorMessage = (runtimeException != null && runtimeException.getMessage() != null ?
                runtimeException.getMessage() : "Unknown error.");
        model.addAttribute("message", errorMessage);
        model.addAttribute("statusCode", HttpStatus.CONFLICT.value());
        model.addAttribute("codeTitle", HttpStatus.CONFLICT.getReasonPhrase());
        return "error";
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public String exceptionEntityNotFoundException (RuntimeException runtimeException, Model model) {
        String erorMessage = (runtimeException != null && runtimeException.getMessage() != null ?
                runtimeException.getMessage() : "Unknown error.");
        model.addAttribute("message", erorMessage);
        model.addAttribute("statusCode", HttpStatus.NOT_FOUND.value());
        model.addAttribute("codeTitle", HttpStatus.NOT_FOUND.getReasonPhrase());
        return "error";
    }
}
