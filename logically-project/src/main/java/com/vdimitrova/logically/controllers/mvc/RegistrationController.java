package com.vdimitrova.logically.controllers.mvc;

import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.models.dto.UserDtoRegistration;
import com.vdimitrova.logically.models.mappers.UserMapper;
import com.vdimitrova.logically.services.helpers.contracts.UserDetailsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

import java.util.List;

import static com.vdimitrova.logically.utils.GlobalConstants.*;

@Controller
public class RegistrationController {

    private final UserDetailsManager userDetailsManager;
    private final UserMapper userMapper;
    private final UserDetailsHelper userDetailsHelper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(
            UserDetailsManager userDetailsManager,
            UserMapper userMapper,
            UserDetailsHelper userDetailsHelper,
            PasswordEncoder passwordEncoder) {
        this.userDetailsManager = userDetailsManager;
        this.userMapper = userMapper;
        this.userDetailsHelper = userDetailsHelper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDtoRegistration", new UserDtoRegistration());
        return "register";
    }

    @PostMapping("register")
    public String registerUser(
            @Valid @ModelAttribute("userDtoRegistration") UserDtoRegistration userDtoRegistration,
            BindingResult bindingResult,
            Model model) {

        if (userDetailsManager.userExists(userDtoRegistration.getUsername())) {
            model.addAttribute("userDtoRegistration", userDtoRegistration);
            model.addAttribute("error", String.format(USERNAME_EXISTS_ERROR, userDtoRegistration.getUsername()));
            return "register";
        }
        if (userDetailsHelper.isExist(userDtoRegistration.getEmail())) {
            model.addAttribute("userDtoRegistration", userDtoRegistration);
            model.addAttribute("error", String.format(EMAIL_EXISTS_ERROR, userDtoRegistration.getEmail()));
            return "register";
        }
        if (!userDtoRegistration.getPassword().equals(userDtoRegistration.getPasswordConfirmation())) {
            model.addAttribute("userDtoRegistration", userDtoRegistration);
            model.addAttribute("error", PASSWORD_DOESNT_MATCH_ERROR);
            return "register";
        }
        if (bindingResult.hasErrors()) {
            model.addAttribute("userDtoRegistration", userDtoRegistration);
            model.addAttribute("error", EMPTY_FIELDS_ERROR);
            return "register";
        }

        UserDetails userDetails = userMapper.toUserDetails(userDtoRegistration);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        User newUser = new User(
                userDtoRegistration.getUsername(),
                passwordEncoder.encode(userDtoRegistration.getPassword()),
                true,
                true,
                true,
                true,
                authorities);

        userDetailsHelper.register(newUser, userDetails);
        return "register-confirmation";
    }

    @GetMapping("register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }
}
