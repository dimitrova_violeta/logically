package com.vdimitrova.logically.controllers.mvc;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.dto.ProblemDtoUserSolution;
import com.vdimitrova.logically.services.contracts.ProblemService;
import com.vdimitrova.logically.services.contracts.UserDetailsService;
import com.vdimitrova.logically.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/problems")
public class ProblemController {

    private final ProblemService problemService;
    private final UserService userService;
    private final UserDetailsService userDetailsService;

    @Autowired
    public ProblemController(
            ProblemService problemService,
            UserService userService,
            UserDetailsService userDetailsService) {
        this.problemService = problemService;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @GetMapping
    public String showAllProblems(Model model) {
        List<Problem> problems = problemService.getAll();
        model.addAttribute("problems", problems);
        return "problems/problems";
    }

    @GetMapping("/{id}")
    public String showProblemById(@PathVariable int id, Model model) {
        Problem problem = problemService.getById(id);
        model.addAttribute("problem", problem);
        model.addAttribute("problemDtoUserSolution", new ProblemDtoUserSolution());
        return "problems/problemDetails";
    }

    @PostMapping("/{id}")
    public String checkUserSolution(
            @ModelAttribute("problemDtoUserSolution") ProblemDtoUserSolution problemDtoUserSolution,
            @PathVariable int id,
            Model model,
            Principal principal) {
        Problem problem = problemService.getById(id);
        String solutionMessage;
        if (principal != null) {
            User user = userService.getByUsername(principal.getName());
            solutionMessage = userDetailsService.checkUserSolution(problem,
                    problemDtoUserSolution.getUserSolution(),
                    user.getUserDetails());
        } else {
            solutionMessage = userDetailsService.checkSolution(problem,
                    problemDtoUserSolution.getUserSolution());
        }
        model.addAttribute("solutionMessage", solutionMessage);
        model.addAttribute("problem", problem);
        return "problems/problemDetails";
    }
}
