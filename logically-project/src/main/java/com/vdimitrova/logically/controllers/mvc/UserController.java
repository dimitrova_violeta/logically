package com.vdimitrova.logically.controllers.mvc;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.models.dto.UserDtoEditProfile;
import com.vdimitrova.logically.models.mappers.UserMapper;
import com.vdimitrova.logically.services.contracts.UserDetailsService;
import com.vdimitrova.logically.services.contracts.UserService;
import com.vdimitrova.logically.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

import static com.vdimitrova.logically.utils.GlobalConstants.EMPTY_FIELDS_ERROR;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    private final UserDetailsService userDetailsService;
    private final UserMapper userMapper;

    @Autowired
    public UserController(UserService userService, UserDetailsService userDetailsService, UserMapper userMapper) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.userMapper = userMapper;
    }

    @GetMapping
    public String showUsers(Model model) {
        List<User> users = userService.getAllRoleUser();
        model.addAttribute("users", users);
        return "users/users";
    }

    @GetMapping("/profile")
    public String showUserProfile(Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        model.addAttribute("user", user);
        model.addAttribute("roles", user.getAuthorities());
        model.addAttribute("imageUtils", new ImageUtils());
        return "users/userProfile";
    }

    @GetMapping("/edit")
    public String showUserProfileEdit(Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        UserDtoEditProfile userDtoEdit = new UserDtoEditProfile();

        userDtoEdit.setFirstName(user.getUserDetails().getFirstName());
        userDtoEdit.setLastName(user.getUserDetails().getLastName());
        userDtoEdit.setEmail(user.getUserDetails().getEmail());

        model.addAttribute("userDtoEdit", userDtoEdit);
        model.addAttribute("userPicture", user.getUserDetails().getPicture());
        model.addAttribute("imageUtils", new ImageUtils());

        return "users/userProfileEdit";
    }

    @PostMapping("/edit")
    public String updateUserProfile(@Valid @ModelAttribute("userDtoEdit") UserDtoEditProfile userDtoEdit,
                                  BindingResult bindingResult,
                                  Model model,
                                  Principal principal) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("userDtoEdit", userDtoEdit);
            model.addAttribute("error", EMPTY_FIELDS_ERROR);
            return "users/userProfileEdit";
        }

        User userToUpdate = userService.getByUsername(principal.getName());
        UserDetails userDetailsToUpdate = userToUpdate.getUserDetails();

        userMapper.toUserDetails(userDetailsToUpdate, userDtoEdit);
        userToUpdate.setUserDetails(userDetailsToUpdate);

        userDetailsService.update(userDetailsToUpdate);

        return "redirect:/users/profile";
    }

    @GetMapping("/problems")
    public String showUserProblems(Model model, Principal principal) {
        User user = userService.getByUsername(principal.getName());
        UserDetails userDetails = user.getUserDetails();

        List<Problem> userProblems = userDetailsService.getUserProblems(userDetails);
        model.addAttribute("userProblems", userProblems);

        return "users/userProblems";
    }
}
