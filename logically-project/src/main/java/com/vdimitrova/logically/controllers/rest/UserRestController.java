package com.vdimitrova.logically.controllers.rest;

import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.services.contracts.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserDetailsService userDetailsService;

    @Autowired
    public UserRestController(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping
    public List<UserDetails> getAll() {
        return userDetailsService.getAll();
    }

    @GetMapping("/{id}")
    public UserDetails getById(@PathVariable int id) {
        return userDetailsService.getById(id);
    }
}
