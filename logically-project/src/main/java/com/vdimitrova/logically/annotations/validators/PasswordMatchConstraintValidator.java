package com.vdimitrova.logically.annotations.validators;

import com.vdimitrova.logically.annotations.PasswordMatch;
import org.apache.commons.beanutils.BeanUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchConstraintValidator implements ConstraintValidator<PasswordMatch, Object> {

    private String password;
    private String passwordConfirmation;

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        boolean valid;
        try {
            final Object firstObject = BeanUtils.getProperty(value, password);
            final Object secondObject = BeanUtils.getProperty(value, passwordConfirmation);

            valid = firstObject == null && secondObject == null ||
                    firstObject != null && firstObject.equals(secondObject);
        } catch (final Exception ignore) {
            return false;
        }
        return valid;
    }

    @Override
    public void initialize(PasswordMatch constraintAnnotation) {
        password = constraintAnnotation.firstPassword();
        passwordConfirmation = constraintAnnotation.secondPassword();
    }
}
