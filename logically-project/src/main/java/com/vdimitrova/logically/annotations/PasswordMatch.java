package com.vdimitrova.logically.annotations;

import com.vdimitrova.logically.annotations.validators.PasswordMatchConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordMatchConstraintValidator.class)
public @interface PasswordMatch {

    String message() default "Password does not match!";

    String firstPassword();

    String secondPassword();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
