package com.vdimitrova.logically.annotations.validators;

import com.vdimitrova.logically.annotations.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordConstraintValidator implements ConstraintValidator<Password, String> {

    private Pattern pattern;

    private static final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})";

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    @Override
    public void initialize(Password constraintAnnotation) {
        pattern = Pattern.compile(PASSWORD_PATTERN);
    }
}
