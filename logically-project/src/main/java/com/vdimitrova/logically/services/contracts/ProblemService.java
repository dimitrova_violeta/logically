package com.vdimitrova.logically.services.contracts;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.UserDetails;

import java.util.List;

public interface ProblemService {

    List<Problem> getAll();

    Problem getById(int id);
}
