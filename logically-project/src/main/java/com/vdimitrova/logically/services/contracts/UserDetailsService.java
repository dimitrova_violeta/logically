package com.vdimitrova.logically.services.contracts;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;

import java.util.List;

public interface UserDetailsService {

    List<UserDetails> getAll();

    UserDetails getById(int id);

    void create(UserDetails userDetails);

    List<Problem> getSolvedProblems(int id);

    Problem getSolvedProblem(int userId, int problemId);

    String checkUserSolution(Problem problem, String userSolution, UserDetails userDetails);

    String checkSolution(Problem problem, String userSolution);

    void update(UserDetails userDetailsToUpdate);

    List<Problem> getUserProblems(UserDetails userDetails);
}
