package com.vdimitrova.logically.services.helpers;

import com.vdimitrova.logically.exceptions.DuplicateEntityException;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.repositories.contracts.UserDetailsRepository;
import com.vdimitrova.logically.services.contracts.UserDetailsService;
import com.vdimitrova.logically.services.helpers.contracts.UserDetailsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import static com.vdimitrova.logically.utils.GlobalConstants.EMAIL_EXISTS_ERROR;
import static com.vdimitrova.logically.utils.GlobalConstants.USERNAME_EXISTS_ERROR;

@Component
public class UserDetailsHelperImpl implements UserDetailsHelper {

    private final UserDetailsManager userDetailsManager;
    private final UserDetailsService userDetailsService;
    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserDetailsHelperImpl(UserDetailsManager userDetailsManager, UserDetailsService userDetailsService, UserDetailsRepository userDetailsRepository) {
        this.userDetailsService = userDetailsService;
        this.userDetailsManager = userDetailsManager;
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    @Transactional
    public void register(User newUser, UserDetails newUserDetails) {
        validateUniqueUsername(newUserDetails.getUsername());
        validateUniqueEmail(newUserDetails.getEmail());
        userDetailsManager.createUser(newUser);
        userDetailsService.create(newUserDetails);
    }

    @Override
    public boolean isExist(String email) {
        return userDetailsRepository.isExist(email);
    }

    private void validateUniqueUsername(String username) {
        if(!userDetailsRepository.filterByUsername(username).isEmpty()) {
            throw new DuplicateEntityException(String.format(USERNAME_EXISTS_ERROR, username));
        }
    }

    private void validateUniqueEmail(String email) {
        if(!userDetailsRepository.filterByEmail(email).isEmpty()) {
            throw new DuplicateEntityException(String.format(EMAIL_EXISTS_ERROR, email));
        }
    }
}
