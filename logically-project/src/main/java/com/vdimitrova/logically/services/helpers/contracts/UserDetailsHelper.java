package com.vdimitrova.logically.services.helpers.contracts;

import com.vdimitrova.logically.models.UserDetails;
import org.springframework.security.core.userdetails.User;

public interface UserDetailsHelper {

    void register (User newUser, UserDetails newUserDetails);

    boolean isExist(String email);
}
