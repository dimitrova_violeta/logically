package com.vdimitrova.logically.services;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.SolvedProblemsUsers;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.repositories.contracts.UserDetailsRepository;
import com.vdimitrova.logically.services.contracts.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

import static com.vdimitrova.logically.utils.GlobalConstants.*;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }

    @Override
    public List<UserDetails> getAll() {
        return userDetailsRepository.getAll();
    }

    @Override
    public UserDetails getById(int id) {
        return userDetailsRepository.getById(id);
    }

    @Override
    public void create(UserDetails userDetails) {
        userDetailsRepository.create(userDetails);
    }

    @Override
    public List<Problem> getSolvedProblems(int id) {
        return userDetailsRepository.getSolvedProblems(id);
    }

    @Override
    public Problem getSolvedProblem(int userId, int problemId) {
        return userDetailsRepository.getSolvedProblem(userId, problemId);
    }

    @Override
    public String checkUserSolution(Problem problem, String userSolution, UserDetails userDetails) {
        String solutionMessage;
        if (problem.getSolution().equalsIgnoreCase(userSolution)) {
            if (getSolvedProblems(userDetails.getId()).stream().noneMatch(p -> p.getId() == problem.getId())) {
                addProblemToUser(problem, userDetails);
                changeUserScore(problem, userDetails);
                solutionMessage = String.format(CORRECT_SOLUTION_MESSAGE, problem.getPoints());
            } else {
                solutionMessage = SOLUTION_MESSAGE;
            }
        } else {
            solutionMessage = INCORRECT_SOLUTION_MESSAGE;
        }
        return solutionMessage;
    }

    @Override
    public String checkSolution(Problem problem, String userSolution) {
        String solutionMessage;
        if (problem.getSolution().equalsIgnoreCase(userSolution)) {
            solutionMessage = SOLUTION_MESSAGE;
        } else {
            solutionMessage = INCORRECT_SOLUTION_MESSAGE;
        }
        return solutionMessage;
    }

    @Override
    public void update(UserDetails userDetailsToUpdate) {
        userDetailsRepository.update(userDetailsToUpdate);
    }

    private void addProblemToUser(Problem problem, UserDetails userDetails) {
        SolvedProblemsUsers solvedProblemToAdd = new SolvedProblemsUsers();
        solvedProblemToAdd.setProblemId(problem.getId());
        solvedProblemToAdd.setUserId(userDetails.getId());
        userDetailsRepository.addProblemToUser(solvedProblemToAdd);
    }

    private void changeUserScore(Problem problem, UserDetails userDetails) {
        int userScore = userDetails.getScore();
        userScore += problem.getPoints();
        userDetails.setScore(userScore);
        userDetailsRepository.changeUserScore(userDetails);
    }

    @Override
    public List<Problem> getUserProblems(UserDetails userDetails) {
        return userDetailsRepository.getUserProblems(userDetails);
    }
}
