package com.vdimitrova.logically.services;

import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.repositories.contracts.UserRepository;
import com.vdimitrova.logically.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllRoleUser() {
        return userRepository.getAllRoleUser();
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }
}
