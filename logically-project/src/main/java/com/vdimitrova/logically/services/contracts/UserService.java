package com.vdimitrova.logically.services.contracts;

import com.vdimitrova.logically.models.User;

import java.util.List;

public interface UserService {

    List<User> getAllRoleUser();

    User getByUsername(String username);
}
