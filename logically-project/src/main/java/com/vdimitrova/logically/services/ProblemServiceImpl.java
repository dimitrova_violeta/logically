package com.vdimitrova.logically.services;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.repositories.contracts.ProblemRepository;
import com.vdimitrova.logically.services.contracts.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProblemServiceImpl implements ProblemService {

    private final ProblemRepository problemRepository;

    @Autowired
    public ProblemServiceImpl(ProblemRepository problemRepository) {
        this.problemRepository = problemRepository;
    }

    @Override
    public List<Problem> getAll() {
        return problemRepository.getAll();
    }

    @Override
    public Problem getById(int id) {
        return problemRepository.getById(id);
    }
}
