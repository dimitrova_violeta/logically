package com.vdimitrova.logically.repositories.contracts;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.UserDetails;

import java.util.List;

public interface ProblemRepository {

    List<Problem> getAll();

    Problem getById(int id);
}
