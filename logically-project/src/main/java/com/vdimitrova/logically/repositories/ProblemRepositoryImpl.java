package com.vdimitrova.logically.repositories;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.repositories.contracts.ProblemRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProblemRepositoryImpl implements ProblemRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ProblemRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Problem> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Problem> query = session.createQuery("from Problem", Problem.class);
            return query.list();
        }
    }

    @Override
    public Problem getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Problem problem = session.get(Problem.class, id);
            return problem;
        }
    }
}
