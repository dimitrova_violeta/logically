package com.vdimitrova.logically.repositories.contracts;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.SolvedProblemsUsers;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;

import java.util.List;

public interface UserDetailsRepository {

    List<UserDetails> getAll();

    UserDetails getById(int id);

    List<UserDetails> filterByUsername(String username);

    List<UserDetails> filterByEmail(String email);

    boolean isExist(String email);

    void create(UserDetails userDetails);

    List<Problem> getSolvedProblems(int id);

    Problem getSolvedProblem(int userId, int problemId);

    void changeUserScore(UserDetails userDetails);

    void addProblemToUser(SolvedProblemsUsers solvedProblemsUsers);

    void update(UserDetails userDetailsToUpdate);

    List<Problem> getUserProblems(UserDetails userDetails);
}
