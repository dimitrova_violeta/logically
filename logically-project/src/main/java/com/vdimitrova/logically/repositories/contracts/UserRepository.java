package com.vdimitrova.logically.repositories.contracts;

import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;

import java.util.List;

public interface UserRepository {

    List<User> getAllRoleUser();

    public List<User> filterByUsername(String username);

    User getByUsername(String username);
}
