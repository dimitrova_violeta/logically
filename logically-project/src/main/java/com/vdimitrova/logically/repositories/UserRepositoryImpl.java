package com.vdimitrova.logically.repositories;

import com.vdimitrova.logically.exceptions.EntityNotFoundException;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.vdimitrova.logically.utils.GlobalConstants.USERNAME_NOT_FOUND_ERROR;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAllRoleUser() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("select u from User u, Authority a " +
                            "where (a.authority = 'ROLE_USER') and u.username = a.username order by u.id",
                    User.class);
            return query.list();
        }
    }

    @Override
    public List<User> filterByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username",
                    User.class);
            query.setParameter("username", username);
            return query.list();
        }
    }

    @Override
    public User getByUsername(String username) {
        List<User> users = filterByUsername(username);

        if(users.isEmpty()) {
            return null;
        }
        return users.get(0);
    }
}
