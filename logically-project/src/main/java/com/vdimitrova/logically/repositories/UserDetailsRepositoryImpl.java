package com.vdimitrova.logically.repositories;

import com.vdimitrova.logically.models.Problem;
import com.vdimitrova.logically.models.SolvedProblemsUsers;
import com.vdimitrova.logically.models.User;
import com.vdimitrova.logically.models.UserDetails;
import com.vdimitrova.logically.repositories.contracts.UserDetailsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserDetailsRepositoryImpl implements UserDetailsRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserDetailsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("select u from UserDetails u, Authority a " +
                    "where (a.authority = 'ROLE_USER' or a.authority = 'ROLE_MASTER') and u.username = a.username",
                    UserDetails.class);
            return query.list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails userDetails = session.get(UserDetails.class, id);
            return userDetails;
        }
    }

    @Override
    public List<UserDetails> filterByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where username = :username", UserDetails.class);
            query.setParameter("username", username);
            return query.list();
        }
    }

    @Override
    public List<UserDetails> filterByEmail(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where email = :email", UserDetails.class);
            query.setParameter("email", email);
            return query.list();
        }
    }

    @Override
    public boolean isExist(String email) {
        return !filterByEmail(email).isEmpty();
    }

    @Override
    public void create(UserDetails userDetails) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Problem> getSolvedProblems(int id) {
        UserDetails userDetails = getById(id);
        try(Session session = sessionFactory.openSession()) {
            Query<Problem> query = session.createQuery("select distinct p from Problem p, SolvedProblemsUsers s " +
                    "where s.userId = :userId and p.id = s.problemId", Problem.class);
            query.setParameter("userId", userDetails.getId());
            return query.list();
        }
    }

    @Override
    public Problem getSolvedProblem(int userId, int problemId) {
        try(Session session = sessionFactory.openSession()) {
            Query<Problem> query = session.createQuery("select p from Problem p, SolvedProblemsUsers s " +
                    "where s.userId = :userId and s.problemId = :problemId", Problem.class);
            query.setParameter("userId", userId);
            query.setParameter("problemId", problemId);
            return query.list().get(0);
        }
    }

    @Override
    public void changeUserScore(UserDetails userDetails) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetails);
            session.getTransaction().commit();
        }
    }

    @Override
    public void addProblemToUser(SolvedProblemsUsers solvedProblemsUsers) {
        try (Session session = sessionFactory.openSession()) {
            session.save(solvedProblemsUsers);
        }
    }

    @Override
    public void update(UserDetails userDetailsToUpdate) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userDetailsToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<Problem> getUserProblems(UserDetails userDetails) {
        try(Session session = sessionFactory.openSession()) {
            Query<Problem> query = session.createQuery("select p from Problem p " +
                    "where p.creator.id = :userId", Problem.class);
            query.setParameter("userId", userDetails.getId());
            return query.list();
        }
    }
}
