# Logically

A web application for brain training which has four types of users: public, private, masters and administrators.
## Project Description
Logically is a brain training application for the end-users.
The public part of the application is visible without authentication. It provides the following functionalities:
- Solve tasks
- Creation of a new account and login functionality
  
The users part of the application is for registered users only and is accessible after successful login.
The main functionalities provided for this area are:
- Earning points for solve tasks
- Sending proposal for a new task
 
The masters part of the application is for registered users only and is accessible after successful login.
The main functionalities provided for this area are:
- Approval or rejection of the proposed tasks

The administrators of the system have permission to manage all major information objects in the system.
The main functionalities provided for this area are:
- Add / Delete tasks
- Edit / Delete users and masters.

## Database diagram
![database diagram](database/database.JPG) 
